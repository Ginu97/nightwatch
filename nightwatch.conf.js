const chromedriver = require('chromedriver');
const geckodriver = require('geckodriver');


module.exports = {
  page_objects_path: "test/page-objects",
  step_definitions: "test/step-definitions",
  src_folders: "test/features",
  test_settings: {
    default: {
      launch_url: 'http://localhost:3000/login',
      webdriver: {
        start_process: true,
        port: 4447
      },
      screenshots: {
        enabled: true,
        path: 'tmp/reports/screenshots'
      }
    },
    chromeHeadless: {
      webdriver: {
        server_path: chromedriver.path,
        cli_args: ['--port=4447']
      },
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
        chromeOptions: {
          args: ["--headless", 'disable-gpu',"--no-sandbox"],
          // binary:"/usr/bin/google-chrome"
        }
      }
    },
    chrome: {
      webdriver: {
        server_path: chromedriver.path,
        cli_args: ['--port=4447']
      },
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
        chromeOptions: {
          args: ['disable-gpu',"start-maximized","--no-sandbox","--no-cache",'--disable-extensions', '--disable-dev-shm-usage']
        }
      }
    },
    firefox: {
      webdriver: {
        server_path: geckodriver.path,
        cli_args: ['--port', '4447', '--log', 'debug']
      },
      desiredCapabilities: {
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true,
        marionette: true
      }
    }
  }
};

