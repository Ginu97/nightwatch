Feature: Login Function

As a user of the company
I want to Login
And I can access the account functions

Scenario: Checking the Login page contents

Given I am on the Login page
Then the page contains a logo at the top
And the page contains a header
And the page contains two form widgets

Scenario: Checking the contents of first login form

Given I am on the login form
And the Login form exists
And the form contains a header and text
And the form contains the fields Email address, password or pin and submit button
And the form contains links at bottom

Scenario: Checking the contents of second login form

Given I am on the login form
And the Login form exists
And the form contains a header and text
And the form contains the fields Email address, password or pin and submit button
And the form contains links at bottom

Scenario Outline: Unsuccessful login with Email and Password

Given I am on the login form
When the user logs in using Username as "<EMAIL>" and Password as "<PASSWORD>"
And the user clicks on submit button
Then login fails

Examples:
    | EMAIL              | PASSWORD  |
    |                    |           |
    |                    | Pass123#1 |
    | ginu@toobler.com   |           |
    | ginu123@toobler.com|Pass123    |
    | ginu               |           |
    | ginu@toobler.com   | pass      |

Scenario Outline: Successful login with Email and password

Given the Login form exists
When the user logs in using Username as "<EMAIL>" and Password as "<PASSWORD>"
And the user clicks on submit button
Then the login is Successful

Examples:
    | EMAIL             | PASSWORD  | 
    | ginu@toobler.com  | Pass123#1 | 

Scenario Outline: Unsuccessful login with Email and Pin

Given the Login form exists
When the user logs in using Username as "<EMAIL>" and Password as "<PIN>"
And the user clciks on submit button
Then login fails

Examples:
    | EMAIL                 | PIN       |
    |                       |           |
    |                       | 1111      |
    | ginumk@mailinator.com |           |
    | ginu@toobler.com      | Pass123#1 |
    | ginum1@mailinator.com | Pass123#1 |

Scenario Outline: Successful login with Email and Pin

Given the Login form exists
When the user logs in using Username as "<EMAIL>" and Password as "<PIN>"
And the user clicks on submit button
Then the login is Successful

Examples: 
    | EMAIL                 | PIN       |
    | ginuk1@mailinator.com | 1111      |


